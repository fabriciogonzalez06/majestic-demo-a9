import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

declare function cargar();

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public anio=new Date().getFullYear();

  constructor(public router:Router ) {


  }

  ngOnInit(): void {
    if(location.reload){
      return;
    }else{

      cargar();
    }
  }


  login(){

    localStorage.setItem("token","hay token");
    this.router.navigateByUrl('/');
    //window.location.reload();
  }

}
