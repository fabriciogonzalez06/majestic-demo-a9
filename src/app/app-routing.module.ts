import { NgModule } from '@angular/core';
import {Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './services/guards/auth.guard';




const ROUTES:Routes=[

  {
      path:'login',
      component:LoginComponent,
      data:{title:'Login'}
  },
    {
        path:'',
        component:AdminComponent,
        canActivate:[AuthGuard],
        children:[
            {
                path:'ejem',
                loadChildren: ()=> import('./views/ejemplo/ejemplo.module').then(m=>m.EjemploModule)
            }
        ]
    },
    {
      path:'**',
      component:LoginComponent
    }
];



@NgModule({

    imports:[
        RouterModule.forRoot(ROUTES,{useHash:true})
    ],
    exports:[
        RouterModule
    ]
})

export class  AppRoutingModule{}
