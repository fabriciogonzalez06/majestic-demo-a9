import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ejemplo2Component } from './ejemplo2/ejemplo2.component';
import { Ejemplo1Component } from './ejemplo1/ejemplo1.component';
import { EjemploRoutingModule } from './ejemplo-routing.module';



@NgModule({
  declarations: [
    Ejemplo1Component,
    Ejemplo2Component
  ],
  imports: [
    CommonModule,
    EjemploRoutingModule
  ]
})
export class EjemploModule { }
