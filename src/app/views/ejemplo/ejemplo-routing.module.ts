import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Ejemplo1Component } from './ejemplo1/ejemplo1.component';
import { Ejemplo2Component } from './ejemplo2/ejemplo2.component';


const routes:Routes=[

    {
        path:'ejemplo1',
        component:Ejemplo1Component,
        data:{title:'Ejemplo 1'}
    },
    {
        path:'ejemplo2',
        component:Ejemplo2Component,
        data:{title:'Ejemplo 2'}
    }
];



@NgModule({
    imports:[
        RouterModule.forChild(routes)
    ],
    exports:[
        RouterModule
    ]
})



export class EjemploRoutingModule{}