import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  public menu:any[]=[];
  constructor() { }


  generarMenu(){


    this.menu=[
      {
        texto:'Prueba',
        icono:'fa fa-users',
        urlTemplate:null,
        activo:true,
        subItems:[
          {
            texto:'Ejemplo 1',
            icono:'fa fa-users',
            urlTemplate:'/ejem/ejemplo1',
            activo:true,
          },
          {
            texto:'Ejemplo 2',
            icono:'fa fa-users',
            urlTemplate:'/ejem/ejemplo2',
            activo:true,
          }

        ]
      },
      {
        texto:'Prueba',
        icono:'fa fa-users',
        urlTemplate:null,
        activo:true,
        subItems:[
          {
            texto:'Ejemplo 1',
            icono:'fa fa-users',
            urlTemplate:'/ejem/ejemplo1',
            activo:true,
          },
          {
            texto:'Ejemplo 2',
            icono:'fa fa-users',
            urlTemplate:'/ejem/ejemplo2',
            activo:true,
          }

        ]
      }
    ];

    return this.menu;

  }



}
