import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

declare function cargar();

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private router:Router){

  }

  canActivate():  boolean {
    if(localStorage.getItem("token")){
      return true;
    }else{
      //cargar();
     //window.location.reload();
    this.router.navigateByUrl('/login');
     return false;
    }
  }

}
