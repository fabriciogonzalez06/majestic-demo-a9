import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { AsideComponent } from './aside/aside.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ProfileComponent } from './profile/profile.component';



@NgModule({
    declarations:[
        NavbarComponent,
        SidebarComponent,
        FooterComponent,
        AsideComponent,
        ProfileComponent
    ],
    imports:[CommonModule,BrowserModule,RouterModule],
    exports:[
        NavbarComponent,
        SidebarComponent,
        FooterComponent,
        AsideComponent
    ]
})

export class SharedModule{}
