import { Component, OnInit } from '@angular/core';
import { EjemploService } from 'src/app/services/ejemplo/ejemplo.service';
import { SidebarService } from 'src/app/services/shared/sidebar.service';

declare function cargar();

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public menu:any[]=[];

  constructor(
    public sidebarService:SidebarService
  ) { }

  ngOnInit(): void {

    cargar();

    this.menu= this.sidebarService.generarMenu();
    console.log(this.menu);
  }

}
