import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { AdminComponent } from './admin/admin.component';
import { ServicesModule } from './services/services.module';
import { RouterModule } from '@angular/router';

const CONTAINERS=[
AdminComponent

];



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
   ...CONTAINERS
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ServicesModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
