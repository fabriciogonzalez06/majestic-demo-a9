import { Component, OnInit } from '@angular/core';

declare function cargar();


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {


  constructor() {

  }

  ngOnInit(): void {
    cargar();

  }


  oadExternalScript (scriptURL){
    return new Promise(resolve => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptURL;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }







}
